<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'MainController@home');	

Route::get('/carrito', 'ShoppingCartsController@index');

Route::get('/market', 'Marketcontroller@ver');

Auth::routes();

Route::resource('products','ProductsController');

Route::resource('in_shopping_carts','InShoppingCartsController',['only'=>['store','destroy']]);
/*
GET /products=>index
POST /products=>store
GET /products/create=>Formulario para crear
*/
Route::resource('consult','ConsultsController');

Route::get('/home', 'HomeController@index')->name('home');


Route::get('products/images/{filename}',function($filename){
	$path = storage_path("app/images/$filename");

	if (!\File::exists($path)) abort(404);
	$file = \File::get($path);
	$type = \File::mimeType($path);

	$response = Response::make($file,200);
	$response->header("Content-Type",$type);
	
	return $response;

});