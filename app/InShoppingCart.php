<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InShoppingCart extends Model
{
    
	//en el carrito de compras(para que el carrito de compras no sea cambiado a comprado)
	protected $fillable =["product_id","shopping_cart_id"];

}
