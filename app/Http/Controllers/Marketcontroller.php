<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Product;



class Marketcontroller extends Controller
{
	public function ver(){
	    $products = Product::all();
	    return view("market.view",["products"=>$products]);
	}
	public function show($id)
    {
        //muestra el producto vista del cliente
        $product= Product::find($id);
        return view('products.show',['product'=>$product]);
    }
}
