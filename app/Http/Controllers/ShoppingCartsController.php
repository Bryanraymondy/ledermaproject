<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\ShoppingCart;

class ShoppingCartsController extends Controller
{
	//mostrar los productos del carrito en una vista particular
    public function index(){

            $shopping_card_id =\Session::get('shopping_card_id');

            $shopping_cart = ShoppingCart::FindOrCreateBySessionID($shopping_card_id);

            $products = $shopping_cart->products()->get();

            $total = $shopping_cart->total();
            //el manejo de datos se hace en el modelo
            //los controladores solo conectan

            return view("shopping_carts.index",["products"=>$products,"total"=>$total]);
            
    }
}
