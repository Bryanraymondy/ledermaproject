<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Product;

use Illuminate\Support\Facades\Auth;

class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function index()
    {
        //muestra la coleccion de productos
        $products = Product::all();
        return view("products.index",["products"=>$products]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //despliega formulario para agregar
        //
        $product= new Product;
        return view("products.create",["product"=>$product]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $hasFile = $request->hasFile('cover') && $request->cover->isValid();

        //se encarga de guardar el nuevo producto
        $product = new Product;

        $product->title = $request->title;
        $product->description = $request->description;
        $product->pricing = $request->pricing;
        
        $product->user_id = Auth::user()->id;

        if ($hasFile) {
            $extension = $request->cover->extension();
            $product->extension = $extension;
        }

        if ($product->save()) {

            if ($hasFile) {
                $request->cover->storeAs('images',"$product->id.$extension");
            }
           return redirect("/products");
        }else{
            //en caso de que no logres enviar se mantiene en la misma pagina
            return view("products.create",["product"=>$product]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //muestra el producto vista del cliente
        $product= Product::find($id);
        return view('products.show',['product'=>$product]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //muestra el formulario
        $product= Product::find($id);
        return view("products.edit",["product"=>$product]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //actualiza el producto
        $product= Product::find($id);

        $product->title = $request->title;
        $product->description = $request->description;
        $product->pricing = $request->pricing;
        
        

        if ($product->save()) {
           return redirect("/products");
        }else{
            //en caso de que no logres enviar se mantiene en la misma pagina
            return view("products.edit",["product"=>$product]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //elimina el producto
        Product::Destroy($id);
        return redirect('/products');

    }
}
