<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    //para indicar a laravel la class con la pluralizacion
    protected $table ='categories';

    //crar la relacion con la tabla user
    public function user(){

    	return $this->belongsTo(User::class);
    }
}
