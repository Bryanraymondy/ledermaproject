<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShoppingCart extends Model
{
	protected $fillable =['status']; 

	public function InShoppingCarts(){
		//se espera una referencia al id
		return $this->hasMany('App\InShoppingCarts');
	}

	public function products(){

		return $this->belongsToMany('App\Product','in_shopping_carts');
	}




	public function productsSize(){
		 return $this->products()->count();
	}

	public function total(){
		return $this->products()->sum("pricing");
	}



    public static function FindOrCreateBySessionID($shopping_cart_id){
    	if ($shopping_cart_id) {
		//buscar el carrito de compras con este id	
	    	return ShoppingCart::findBySession($shopping_cart_id);
	    }
	    else{
	    	//crear un carrito de compras
	    	return ShoppingCart::createWithoutSession();
	    }
    }

    public static function findBySession($shopping_cart_id){
    	return ShoppingCart::find($shopping_cart_id);
    }



    public static function createWithoutSession(){

    	return ShoppingCart::create(["status"=>"incompleted"]);

    	
    }
}
