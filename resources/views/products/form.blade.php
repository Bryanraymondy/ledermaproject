
{!!Form::open(['url' => $url ,'method' => $method , 'files'=> true])!!}

			<div class="form-group">{{Form::text('title',$product->title,['class'=>'form-control','placeholder'=>'Título...'])}}</div>
	
			<div class="form-group"><?php
        $opciones = ['1' => 'cabello', '2' => 'piel', '3' => 'pies'];
        $seleccionadas = ['1']; 
    ?>

    {{ Form::select('mi_select', $opciones, $seleccionadas) }} </div>	

			<div class="form-group">{{Form::number('pricing',$product->pricing,['class'=>'form-control','placeholder'=>'Precio de tu producto en dolares'])}}</div>
			<div class="form-group">
				{{  Form::file('cover')  }}
			</div>
			<div class="form-group">{{Form::textarea('description',$product->description,['class'=>'form-control','placeholder'=>'Describe tu Producto'])}}</div>
			<!--<div class="form-group">{{Form::file('photo',$product->photo,['class'=>'form-control','placeholder'=>'Foto...'])}}</div>-->
			<div class="form-group text-right">
				<a href="{{ url('/products') }}"> Regresar al listado de productos</a>
				<input type="submit" value="Guardar" class="btn btn-success">
			</div>
{!!Form::close()!!}