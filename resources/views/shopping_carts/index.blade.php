@extends("layouts.app")
@section("content")
	<div class="big-paddin text-center blue-grey white-texts">
		<h1>Tu carrito de compras</h1>

	</div>
	<div class="container">
		<table class="table table-bordered">
			<thead>
				<tr>
					<td>Producto</td>
					<td>Precio</td>
				</tr>
			</thead>
			<tbody>
				@foreach($products as $product)
				<tr>
					<td>{{$product->title}}</td>
					<td>{{$product->pricing}}</td>
				</tr>

				@endforeach
				<tr>
					<td>Total</td>
					<td>{{$total}}</td>
				</tr>
			</tbody>

		</table>
		<a href="">Pagar en Caja</a>
	</div>

@endsection